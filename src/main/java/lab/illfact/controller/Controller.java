package lab.illfact.controller;

import java.io.IOException;
import java.util.ArrayList;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import lab.illfact.model.DAO;
import lab.illfact.model.Record;

@WebServlet(urlPatterns = {"/Controller", "/main", "/insert", "/select", "/update", "/delete"})
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	DAO dao = new DAO();
	Record record = new Record();
	
    public Controller() {
    
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getServletPath();
		System.out.println(action);
		
		if(action.equals("/main")) {
			collection(request, response);
		} else if(action.equals("/insert")) {
			createLp(request, response);
		} else if(action.equals("/select")) {
			listLp(request, response);
		} else if(action.equals("/update")) {
			updateLp(request, response);
		} else if(action.equals("/delete")) {
			deleteLp(request, response);
		} else {
			response.sendRedirect("index.html");
		}
		
	}
	
	// List LPs
	protected void collection(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<Record> records = dao.listRecords();
		request.setAttribute("Records", records);
		RequestDispatcher rd = request.getRequestDispatcher("collection.jsp");
		rd.forward(request, response);
		
	}
	
	// Create LP
	protected void createLp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		record.setName(request.getParameter("name"));
		record.setArtist(request.getParameter("artist"));
		record.setRelease_year(Integer.parseInt(request.getParameter("release_year")));
		
		// Call inserRecod method passing the Record object
		dao.insertRecord(record);
		response.sendRedirect("main");
		
	}
	
	// Select LP
	protected void listLp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		record.setId(id);
		dao.listRecord(record);
		
		request.setAttribute("id", record.getId());
		request.setAttribute("name", record.getName());
		request.setAttribute("artist", record.getArtist());
		request.setAttribute("release_year", record.getRelease_year());
		
		RequestDispatcher rd = request.getRequestDispatcher("update.jsp");
		rd.forward(request, response);
		
	}
	
	protected void updateLp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		record.setId(Integer.parseInt(request.getParameter("id")));
		record.setName(request.getParameter("name"));
		record.setArtist(request.getParameter("artist"));
		record.setRelease_year(Integer.parseInt(request.getParameter("release_year")));
		
		dao.updateRecord(record);
		response.sendRedirect("main");
		
	}
	
	protected void deleteLp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		record.setId(Integer.parseInt(request.getParameter("id")));
		
		dao.deleteRecord(record);
		response.sendRedirect("main");
	}
	
}
