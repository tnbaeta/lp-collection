package lab.illfact.model;

public class Record {

	private int id;
	private String name;
	private String artist;
	private int release_year;
	
	public Record() {
		super();
	}

	public Record(int id, String name, String artist, int release_year) {
		super();
		this.id = id;
		this.name = name;
		this.artist = artist;
		this.release_year = release_year;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public int getRelease_year() {
		return release_year;
	}

	public void setRelease_year(int release_year) {
		this.release_year = release_year;
	}
	
	
}
