package lab.illfact.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class DAO {
	/** Connection module **/
	// Connection parameters
	private String driver = "com.mysql.cj.jdbc.Driver";
	private String url = "jdbc:mysql://localhost:3306/lp_collection?useTimezone=true&serverTimezone=UTC";
	private String username = "root";
	private String password = "VMware1!";
	
	// Connection method
	private Connection connect() {
		Connection conn = null;
		
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url, username, password);
			return conn;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}
	
	/** CRUD CREATE **/
	public void insertRecord(Record record) {
		String create = "insert into records (name,artist,release_year) values (?,?,?)";
		
		try {
			Connection conn = connect();
			PreparedStatement ps = conn.prepareStatement(create);
			ps.setString(1, record.getName());
			ps.setString(2, record.getArtist());
			ps.setInt(3, record.getRelease_year());
			
			ps.executeUpdate();
			conn.close();
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	/** CRUD READ **/
	public ArrayList<Record> listRecords() {
		String read = "select * from records order by artist";
		ArrayList<Record> records = new ArrayList<>();
		try {
			Connection conn = connect();
			PreparedStatement ps = conn.prepareStatement(read);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				String artist = rs.getString(3);
				int release_year = rs.getInt(4);
				records.add(new Record(id, name, artist, release_year));
			}
			conn.close();
			return records;
			
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}
	
	/** CRUD UPDATE **/
	public void listRecord(Record record) {
		String list = "select * from records where id = ?";
		
		try {
			Connection conn = connect();
			PreparedStatement ps = conn.prepareStatement(list);
			ps.setInt(1, record.getId());
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				record.setId(rs.getInt(1));
				record.setName(rs.getString(2));
				record.setArtist(rs.getString(3));
				record.setRelease_year(rs.getInt(4));
			}
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public void updateRecord(Record record) {
		String update = "update records set name=?,artist=?,release_year=? where id=?";
		
		try {
			Connection conn = connect();
			PreparedStatement ps = conn.prepareStatement(update);
			ps.setString(1, record.getName());
			ps.setString(2, record.getArtist());
			ps.setInt(3, record.getRelease_year());
			ps.setInt(4, record.getId());
			ps.executeUpdate();
			conn.close();
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public void deleteRecord(Record record) {
		String delete = "delete from records where id=?";
		
		try {
			Connection conn = connect();
			PreparedStatement ps = conn.prepareStatement(delete);
			ps.setInt(1, record.getId());
			ps.executeUpdate();
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
