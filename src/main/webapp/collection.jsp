<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="lab.illfact.model.Record"%>
<%@ page import="java.util.ArrayList"%>
<%
	@SuppressWarnings("unchecked")
	ArrayList<Record> records = (ArrayList<Record>) request.getAttribute("Records");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>LP Collection</title>
<link rel="icon" href="images/favicon.png">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<a href="/lpcollection">
		<img src="images/vinyl.png" alt="Vinyl Collection">
	</a>
	<h1>LP Collection</h1>
	<a href="new.html" class="Button1">New LP</a>
	<table id="table">
		<thead>
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Artist</th>
				<th>Release Year</th>
				<th>Options</th>
			</tr>
		</thead>
		<tbody>
			<%for (int i = 0; i < records.size(); i++) { %>
				<tr>
					<td><%=records.get(i).getId() %></td>
					<td><%=records.get(i).getName() %></td>
					<td><%=records.get(i).getArtist() %></td>
					<td><%=records.get(i).getRelease_year() %></td>
					<td><a href="select?id=<%=records.get(i).getId() %>" class="Button1">Update</a>
						<a href="javascript: confirmation(<%=records.get(i).getId() %>)" class="Button2">Delete</a>
					</td>
				</tr>
			<%} %>
		</tbody>
	</table>
	<script src="scripts/confirmation.js"></script>

</body>
</html>