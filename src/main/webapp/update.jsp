<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>LP Collection</title>
<link rel="icon" href="images/favicon.png">
<link rel="stylesheet" href="css/style.css">
</head>

<body>
	<h1>Update LP</h1>
	<form name="frmLp" action="update">
		<table>
			<tr>
				<td><input type="text" name="id" class="Box3" readonly value="<%out.print(request.getAttribute("id"));%>"></td>
			</tr>
			<tr>
				<td><input type="text" name="name" class="Box2" value="<%out.print(request.getAttribute("name"));%>"></td>
			</tr>
			<tr>
				<td><input type="text" name="artist" class="Box2" value="<%out.print(request.getAttribute("artist"));%>"></td>
			</tr>
			<tr>
				<td><input type="text" name="release_year" class="Box1" value="<%out.print(request.getAttribute("release_year"));%>"></td>
			</tr>
		</table>
		<input type="button" value="Save" class="Button1" onclick="validate()">
	</form>
<script src="scripts/validator.js"></script>
</body>
</html>