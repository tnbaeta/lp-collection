/**
 * Comfirmation for record deletion 
 */

function confirmation(id) {
	let response = confirm("Confirm the deletion of this record?")
	if (response === true) {
		
		window.location.href = "delete?id=" + id
	}
}