/**
 * Form validation
 *
 */

function validate() {
	let name = frmLp.name.value
	let artist = frmLp.artist.value
	let year = frmLp.release_year.value
	
	if(name === "") {
		alert("The name of the LP cannot be blank!")
		frmLp.name.focus()
		return false
	} else if(artist ==="") {
		alert("The Artist field cannot be blank!")
		frmLp.artist.focus()
		return false
	} else if(year === ""){
		alert("The Year field cannot be blank!")
		frmLp.release_year.focus()
	} else {
		document.forms["frmLp"].submit()
	}
	
}